#!/usr/bin/env bash

set -ex

MODEL_DIR="gs://imfdb-201220-mlengine/output_v$1"
TEST_DATA="$( gsutil ls gs://imfdb-201220-mlengine/data | fgrep imfdb_train_sample -v )"
TRAIN_DATA="$( gsutil ls gs://imfdb-201220-mlengine/data | fgrep imfdb_train_sample | tr '[:space:]' ' ' )"

gcloud ml-engine jobs submit training "imfdb_training_v$1" \
    --package-path trainer/ \
    --module-name trainer.task \
    --job-dir "$MODEL_DIR" \
    --region us-central1 \
    --config ../config/imfdb.yaml \
    -- \
    --train-files "$TRAIN_DATA" \
    --eval-files "$TEST_DATA" \
    --num-epochs 40 \
    --train-steps 50 \
    --batch-size 250 \
    --res-num-outputs 64 \
    --res-steps 4 \
    --res-step-depth 6 \
    --image-height 64 \
    --image-width 64 \
    --learning-rate 0.1 \
    --fc-size 1000
