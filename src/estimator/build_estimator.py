import tensorflow as tf

def build_model_fn(hidden_layers_fn):
    """
    Constructs a valid model_fn for use in an Estimator
    :param hidden_layers_fn: The function that will accept the
    mnist input layer, along with an optional params containing
    the following hyper parameters, and any hyperparameters to
    be passed to hidden_layers_fn. Note: hidden_layers_fn must
    be able to accept the params passed into the returned model_fn.
    The following are hyperparameters currently supported:
        learning_rate: Self-Explanatory

    :return: A model_fn that takes a params argument,
    this is to be used with an Estimator.
    """

    def model_fn(features, labels, mode, params):
        input_layer = tf.reshape(
            features,
            [-1, *features.shape[1:]]
        )

        is_training = mode == tf.estimator.ModeKeys.TRAIN
        hidden = hidden_layers_fn(input_layer, is_training, params)
        logits = tf.layers.dense(hidden, units=100)

        predictions = {
            'classes': tf.argmax(logits, axis=1),
            'probabilities': tf.nn.softmax(
                logits,
                name='softmax_tensor'
            )
        }

        if mode == tf.estimator.ModeKeys.PREDICT:
            return tf.estimator.EstimatorSpec(
                mode,
                predictions=predictions
            )

        loss = tf.losses.sparse_softmax_cross_entropy(
            labels,
            logits
        )

        if mode == tf.estimator.ModeKeys.TRAIN:
            optimizer = tf.train.GradientDescentOptimizer(
                params.learning_rate,
            )

            update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
            with tf.control_dependencies(update_ops):
                train_op = optimizer.minimize(
                    loss,
                    global_step=tf.train.get_global_step()
                )

            return tf.estimator.EstimatorSpec(
                mode,
                loss=loss,
                train_op=train_op,
            )

        return tf.estimator.EstimatorSpec(
            mode,
            loss=loss,
            eval_metric_ops={
                'accuracy' : tf.metrics.accuracy(
                    labels,
                    predictions['classes']
                )
            }
        )

    return model_fn
