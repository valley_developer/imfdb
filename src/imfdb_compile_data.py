import zipfile
import os
import random
import math
import argparse
import tensorflow as tf

rng = random.SystemRandom()

def build_tfrecords(zip_path, odir, train_ratio=0.9, test_sample_size=100):
    with zipfile.ZipFile(zip_path) as zip_file:
        labels =  build_labels(zip_file)
        train_files, test_files = sample_files(
            zip_file,
            train_ratio=train_ratio
        )

    build_record_files(
        zip_path,
        labels,
        train_files,
        test_sample_size,
        odir,
        train=True
    )
    build_record_files(zip_path, labels, test_files, len(test_files), odir)

def build_labels(zip_file):
    dirs = (
        zinfo.filename
        for zinfo in zip_file.infolist()
    )

    dir_comps = map(
        lambda d : d.split(os.path.sep),
        dirs
    )

    # sort and make a set so members
    # are unique and order is predictable
    names = sorted({
        comps[1]
        for comps in dir_comps
        if len(comps[1]) > 0
        # This is the only directory we want to exclude
        # it is not an actor name
        if comps[1] != '.DS_Store'
    })

    return dict(zip(names, range(len(names))))

def sample_files(zip_file, train_ratio=0.9):
    file_list = [
        zinfo.filename
        for zinfo in zip_file.infolist()
        if zinfo.filename.endswith('jpg')
    ]

    # This list is large use an rng that can
    # generate any permutation.
    rng.shuffle(file_list)

    train_samples =  math.ceil(len(file_list) * train_ratio)

    return file_list[0:train_samples], file_list[train_samples:]

def build_record_files(zip_path, labels, files, records, odir, train=False):
    rng.shuffle(files)
    samples = (
        (i, files[i:min(i + records, len(files))])
        for i in range(0, len(files), records)
    )

    for i, file_list in samples:
        tfrecord_job(zip_path, labels, i // records, file_list, train, odir)

def tfrecord_job(zip_path, labels, sample, files, train, odir):
    tfrecord_name = os.path.join(
        odir,
        "imfdb_{0}{1}.tfrecord".format(
            "train" if train else "test",
            "_sample{}".format(sample) if train else ""
        )
    )

    with zipfile.ZipFile(zip_path) as zip_file:
        with tf.python_io.TFRecordWriter(tfrecord_name) as writer:
            for f in files:
                writer.write(
                    tf.train.Example(
                        features=build_features(zip_file, f, labels)
                    ).SerializeToString()
                )

def build_features(zip_file, file_name, labels):
    label = [(l, labels[l]) for l in labels if l in file_name][0]
    file_bytes = zip_file.read(file_name)

    return tf.train.Features(feature={
        'label_name' : _b_feat(str.encode(label[0])),
        'label' : _i64_feat(label[1]),
        'image' : _b_feat(file_bytes),
    })

def _i64_feat(val):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[val]))

def _b_feat(val):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[val]))

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '--zip-path',
        '-z',
        required=True,
        dest='zip_file',
        type=str
    )

    parser.add_argument(
        '--output-dir',
        '-o',
        default=os.getcwd(),
        dest='output_dir',
        type=str
    )

    parser.add_argument(
        '--train-ratio',
        '-t',
        default=0.9,
        dest='train_ratio',
        type=float
    )

    parser.add_argument(
        '--sample-size',
        '-s',
        default=100,
        dest='sample_size',
        type=int
    )

    args = parser.parse_args()

    build_tfrecords(
        args.zip_file,
        args.output_dir,
        train_ratio=args.train_ratio,
        test_sample_size=args.sample_size,
    )
