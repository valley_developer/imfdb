import tensorflow as tf

def build_train_input_fn(data_path, batch_size, data_size, num_repeats):
    def input_fn():
        return (
            tf.data.TFRecordDataset(data_path)
                .map(_decode)
                .map(_normalize)
                .shuffle(data_size)
                .batch(batch_size)
                .repeat(num_repeats)
        )

    return input_fn

def build_test_input_fn(data_path, batch_size):
    def input_fn():
        return (
            tf.data.TFRecordDataset(data_path)
                .map(_decode)
                .map(_normalize)
                .batch(batch_size)
                .make_one_shot_iterator()
                .get_next()
        )

    return input_fn

def _decode(ser_example):
    features = tf.parse_single_example(
        ser_example,
        features={
            'raw_image' : tf.FixedLenFeature([], tf.string),
            'label' : tf.FixedLenFeature([], tf.int64),
        }
    )

    img = tf.decode_raw(features['raw_image'], tf.uint8)
    img.set_shape([28**2])

    label = features['label']

    return img, label

def _normalize(img, label):
    img = (tf.cast(img, tf.float32) / 255.0) - 0.5

    return img, label
