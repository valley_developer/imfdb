import tensorflow as tf

def resnet(input_layer, filters, kernel_size, res_step_depth, is_training):
    """
    Adds the resnet layers onto the active graph
    """

    last = input_layer
    for i in range(res_step_depth):
        conv1 = tf.layers.conv2d(
            last,
            filters,
            kernel_size,
            strides=((2, 2) if i == 0 else 1),
            padding='same',
            activation=None,
        )
        nconv1 = tf.layers.batch_normalization(conv1, training=is_training)
        anconv1 = tf.nn.relu(nconv1)

        conv2 = tf.layers.conv2d(
            anconv1,
            filters,
            kernel_size,
            padding='same',
            activation=tf.nn.relu,
        )

        if i == 0:
            last = tf.layers.conv2d(
                last,
                filters,
                [1, 1],
                padding='same',
                strides=(2, 2),
            )

        last = tf.nn.relu(tf.add(last, conv2))

    return last

def fully_connected(input_layer, depth, features, is_training):
    """
    Adds fully connected layers to an input network
    """

    last = input_layer
    for _ in range(depth):
        last = tf.contrib.layers.fully_connected(
            last,
            features,
            activation_fn=None,
        )

        last = tf.layers.batch_normalization(last, training=is_training)
        last = tf.nn.relu(last)

    return last
