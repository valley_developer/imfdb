import tensorflow as tf
from nn.layers import resnet, fully_connected
from functools import reduce
from operator import mul

def build_resnet(
    input_layer,
    is_training,
    hyper_params
):
    """
    Builds a resnet on the tf computation graph.

    :param input_layer: Input layer to feed into the nn.
    :param params: params describe some hyper parameters
    currently they are the following:
        res_depth : The number of resnet layers to add after
        an initial convolutional layer.
        res_num_outputs: The number of filters in each convolutional
        layer in the resnet.
        cv_height : The first dimension of the 2d convolutional
        filter to use with the resnet.
        cv_width : The second dimension of the 2d convolutional
        filter to use with the resnet.
        fc_layers : The number of fully connected layers to
        put at the end of the resnet.
        fc_size : The number of dimensions of the fully connected
        layer output vector.
    :return: The last fully connected layer at the end of the resnet
    created by this function.
    """

    conv1 = _add_first_layer(input_layer, hyper_params.res_num_outputs)
    resnet_layers = conv1
    for i in range(hyper_params.res_steps):
        resnet_layers = resnet(
            resnet_layers,
            hyper_params.res_num_outputs * (2**i),
            [hyper_params.cv_height, hyper_params.cv_width],
            hyper_params.res_step_depth,
            is_training,
        )

    avg_pool = tf.layers.average_pooling2d(resnet_layers, [2, 2], 1)
    flat_avg_pool = tf.reshape(avg_pool, [-1, reduce(mul, avg_pool.shape[1:])])
    fc_layers = fully_connected(
        flat_avg_pool,
        hyper_params.fc_layers,
        hyper_params.fc_size,
        is_training,
    )

    return fc_layers

def _add_first_layer(input_layer, filters):
    return tf.layers.conv2d(
        input_layer,
        filters,
        [7, 7],
        padding='same',
        activation=tf.nn.relu,
    )
