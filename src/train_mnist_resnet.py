import tensorflow as tf
import argparse

from estimator import build_model_fn
from nn import resnet
from mnist_data import build_train_input_fn, build_test_input_fn

def parse_argv(argv):
    parser = argparse.ArgumentParser(
        description='This is my mnist tensorflow application'
    )

    parser.add_argument('--train', type=str, dest='train_path', required=True)
    parser.add_argument('--test', type=str, dest='test_path', required=True)
    parser.add_argument(
        '--model',
        type=str,
        dest='model_dir',
        default='/tmp/mnist_resnet_model'
    )
    parser.add_argument(
        '--batch-size',
        type=int,
        dest='batch_size',
        default=300
    )
    parser.add_argument(
        '--learning-rate',
        type=float,
        dest='learning_rate',
        default=0.1
    )
    parser.add_argument(
        '--train-steps',
        type=int,
        dest='train_steps',
        default=1
    )
    parser.add_argument(
        '--res-depth',
        type=int,
        dest='res_depth',
        default=2
    )
    parser.add_argument(
        '--res-num-outputs',
        type=int,
        dest='res_num_outputs',
        default=64
    )
    parser.add_argument(
        '--fc-layers',
        type=int,
        dest='fc_layers',
        default=1
    )
    parser.add_argument(
        '--fc-size',
        type=int,
        dest='fc_size',
        default=1024
    )

    args = parser.parse_args()

    return {
        'train_path' : args.train_path,
        'test_path' : args.test_path,
        'model_dir' : args.model_dir,
        'batch_size' : args.batch_size,
        'learning_rate' : args.learning_rate,
        'train_steps' : args.train_steps,
        'res_depth' : args.res_depth,
        'res_num_outputs' : args.res_num_outputs,
        'fc_layers' : args.fc_layers,
        'fc_size' : args.fc_size,
    }

def main(argv):
    args = parse_argv(argv)

    mnist_classifier = tf.estimator.Estimator(
        build_model_fn(resnet),
        model_dir=args['model_dir'],
        params=args
    )

    mnist_classifier.train(
        steps=args['train_steps'],
        input_fn=build_train_input_fn(
            args['train_path'],
            args['batch_size'],
            50000,
            args['train_steps']
        )
    )

    test_results = mnist_classifier.evaluate(
        input_fn=build_test_input_fn(args['test_path'], args['batch_size'])
    )

    print(test_results)

if __name__ == '__main__':
    tf.app.run()
