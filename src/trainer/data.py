import tensorflow as tf
from functools import partial

def build_train_input_fn(
    train_files,
    batch_size,
    num_repeats,
    img_height,
    img_width,
):
    def input_fn():
        return (
            _build_dataset(train_files, img_height, img_width)
                .shuffle(5000)
                .batch(batch_size)
                .repeat(num_repeats)
        )

    return input_fn

def build_test_input_fn(
    data_path,
    batch_size,
    img_height,
    img_width,
):
    def input_fn():
        return (
            _build_dataset(data_path, img_height, img_width)
                .batch(batch_size)
                .make_one_shot_iterator()
                .get_next()
        )

    return input_fn

def _build_dataset(data, height, width):
    return (
        tf.data.TFRecordDataset(data)
            .map(partial(_decode, height, width))
            .map(_normalize)
    )

def _decode(height, width, ser_example):
    features = tf.parse_single_example(
        ser_example,
        features={
            'image' : tf.FixedLenFeature([], tf.string),
            'label_name' : tf.FixedLenFeature([], tf.string),
            'label' : tf.FixedLenFeature([], tf.int64),
        }
    )

    img = tf.image.decode_jpeg(features['image'], channels=1)
    img = tf.image.resize_images(
        img,
        [height, width],
        method=tf.image.ResizeMethod.BICUBIC
    )

    label = features['label']

    return img, label

def _normalize(img, label):
    img = (tf.cast(img, tf.float32) / 255.0) - 0.5

    return img, label
