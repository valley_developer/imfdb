import argparse
import tensorflow as tf

from estimator import build_model_fn
from nn import resnet
from trainer.data import build_train_input_fn, build_test_input_fn

def build_hparams(argv):
    dct = parse_argv(argv).__dict__

    # Split up all arguments, I was having
    # issues passing the file list to the
    # program in google cloud. It was putting
    # all the files into one argument. This
    # splits up any single argument into it's
    # components, and filters out empty args.
    dct['train_files'] = [
        f
        for arg in dct['train_files']
        for f in arg.split()
        if len(f) > 0
    ]

    return tf.contrib.training.HParams(**dct)

def parse_argv(argv):
    parser = argparse.ArgumentParser(
        description='This is my imfdb tensorflow application'
    )

    parser.add_argument(
        '--eval-files',
        dest='test_files',
        nargs='+',
        required=True,
    )
    parser.add_argument(
        '--train-files',
        dest='train_files',
        nargs='+',
        required=True,
    )
    parser.add_argument(
        '--job-dir',
        type=str,
        dest='model_dir',
        default='/tmp/imfdb_resnet_model'
    )
    parser.add_argument(
        '--batch-size',
        type=int,
        dest='batch_size',
        default=1000
    )
    parser.add_argument(
        '--learning-rate',
        type=float,
        dest='learning_rate',
        default=1.0
    )
    parser.add_argument(
        '--num-epochs',
        type=int,
        dest='num_epochs',
        default=1
    )
    parser.add_argument(
        '--train-steps',
        type=int,
        dest='train_steps',
        default=1
    )
    parser.add_argument(
        '--res-steps',
        type=int,
        dest='res_steps',
        default=3
    )
    parser.add_argument(
        '--res-step-depth',
        type=int,
        dest='res_step_depth',
        default=3
    )
    parser.add_argument(
        '--res-num-outputs',
        type=int,
        dest='res_num_outputs',
        default=64
    )
    parser.add_argument(
        '--cv-height',
        type=int,
        dest='cv_height',
        default=3,
    )
    parser.add_argument(
        '--cv-width',
        type=int,
        dest='cv_width',
        default=3,
    )
    parser.add_argument(
        '--fc-layers',
        type=int,
        dest='fc_layers',
        default=1
    )
    parser.add_argument(
        '--fc-size',
        type=int,
        dest='fc_size',
        default=1000
    )
    parser.add_argument(
        '--image-height',
        type=int,
        dest='img_height',
        default=32,
    )
    parser.add_argument(
        '--image-width',
        type=int,
        dest='img_width',
        default=32,
    )

    return parser.parse_args(argv)

def run(hparams):
    imfdb_classifier = tf.estimator.Estimator(
        build_model_fn(resnet),
        model_dir=hparams.model_dir,
        params=hparams
    )

    for _ in range(hparams.num_epochs):
        imfdb_classifier.train(
            steps=hparams.train_steps,
            input_fn=build_train_input_fn(
                hparams.train_files,
                hparams.batch_size,
                hparams.train_steps,
                hparams.img_height,
                hparams.img_width,
            )
        )

        imfdb_classifier.evaluate(
            input_fn=build_test_input_fn(
                hparams.test_files,
                hparams.batch_size,
                hparams.img_height,
                hparams.img_width,
            )
        )

def main(argv):
    hparams = build_hparams(argv[1:])

    run(hparams)

if __name__ == '__main__':
    tf.app.run()
